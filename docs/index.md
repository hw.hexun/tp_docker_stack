# Liste des services

### Application métier

[Vote app](https://vote.ynov-lab.tk)

[Résultats app](https://result.ynov-lab.tk)

### Supervision

[Grafana](https://monitoring.ynov-lab.tk)

[Prometheus](https://prometheus.ynov-lab.tk)

[Cadvisor](https://cadvisor.ynov-lab.tk)

### Registry

[Harbor](https://harbor.ynov-lab.tk)

### Analyse du code

[Sonarcube](https://sonar.ynov-lab.tk)

# Traefik

Avant de mettre en service notre conteneur Traefik, on doit créer un fichier de configuration et définir un mot de passe chiffré pour pouvoir accéder au tableau de bord de surveillance.

On va utiliser l'utilitaire htpasswd pour créer ce mot de passe. Tout d'abord, installez l'utilitaire, qui est inclus dans le paquet apache2-utils :

``sudo apt-get install apache2-utils``

Générer ensuite un mot de passe chiffré avec la commander suivante

``htpasswd -nb user password``

![Traefik1](img/traefik1.PNG)

Pour configurer le serveur Traefik, on va créer deux nouveaux fichiers de configuration appelés `traefik.toml` et `traefik_dynamic.toml` en utilisant le format TOML. 

TOML est un langage de configuration similaire aux fichiers INI, mais standardisé. Ces fichiers vont nous permettent de configurer le serveur Traefik et les diverses intégrations ou fournisseurs. On va utiliser le fournisseur acme. Celui-ci supporte les certificats TLS en utilisant Let's Encrypt.

Editer ensuite le fichier traefik.toml

``vim traefik.toml``

Il doit ressembler à ça

![Traefik2](img/traefik2.PNG)


Tout d'abord, on vient spécifier les ports que Traefik doit écouter en utilisant la section entryPoints. On veut écouter sur les ports 80 et 443. Appelons-les web (port 80) et websecure (port 443).

Notez qu'on redirige aussi automatiquement le trafic pour qu'il soit traité par TLS.

Ensuite, configurez l'api Traefik, ce qui vous donne accès à la fois à l'API et à l'interface de votre tableau de bord. L'intitulé de `[api]` est tout ce dont on a besoin car le tableau de bord est activé par défaut.

Pour finir de sécuriser nos requêtes web, on souhaite utiliser Let's Encrypt pour générer des certificats TLS valides. Traefik v2 supporte Let's Encrypt par défaut.

On va le configurer en créant un résolveur de certificats de type acme. Cette section est appelée acme car ACME est le nom du protocole utilisé pour communiquer avec Let's Encrypt pour gérer les certificats. 

Le service Let's Encrypt requiert l'enregistrement d'une adresse email valide, donc pour que Traefik génère des certificats pour nos hôtes, il faut définir notre adresse mail. On spécifie ensuite qu'on va stocker les informations qu'on reçoit de Let's Encrypt dans un fichier JSON appelé acme.json.

La section acme.tlsChallenge nous permet de spécifier comment Let's Encrypt peut vérifier que le certificat. On le configurez pour qu'il serve un fichier comme partie du défi sur le port 443.

Enfin, on doit configurer Traefik pour qu'il fonctionne avec Docker.

Notre configuration utilise le fournisseur de fichiers. Avec Traefik v2, les configurations statiques et dynamiques ne peuvent pas être mélangées et assorties. Pour contourner ce problème, on utilise ``traefik.toml`` pour définir nos configurations statiques et on garde nos configurations dynamiques dans un autre fichier  ``traefik_dynamic.toml``. 

Editer ensuite le fichier traefik.toml

``vim traefik_dynamic.toml``

Il doit ressembler à ça

![Traefik-dynamic](img/traefik-dynamic.png)

Pour mettre notre tableau de bord derrière un mot de passe, on doit personnaliser le routeur de l'API et configurer un "middleware" pour gérer l'authentification de base HTTP. 

Le middleware est configuré sur une base par protocole et, puisque vous travaillez avec HTTP, vous le spécifierez comme une section enchaînée de http.middlewares. Vient ensuite le nom de votre middleware afin que vous puissiez le référencer ultérieurement, suivi du type de middleware dont il s'agit, qui sera basicAuth dans ce cas. Notre middleware s'appelle simpleAuth.

Pour configurer le routeur pour l'api, vous devrez à nouveau enchaîner le nom du protocole, mais au lieu d'utiliser http.middlewares, on utilisera http.routers suivi du nom du routeur. 

Dans ce cas, l'api fournit son propre routeur nommé qu'on peut configurer en utilisant la section ``[http.routers.api]``. 
On configure également le domaine qu'on prévoie d'utiliser avec notre tableau de bord en définissant la regle à l'aide d'une correspondance d'hôte, le point d'entrée pour utiliser websecure et les middlewares pour inclure simpleAuth.

Le point d'entrée web gère le port 80, tandis que le point d'entrée websecure utilise le port 443 pour TLS/SSL. On redirige automatiquement tout le trafic sur le port 80 vers le point d'entrée websecure afin de forcer les connexions sécurisées pour toutes les demandes.

Remarquez que les trois dernières lignes configurent un service, activent tls, et configurent certResolver sur "lets-encrypt". Les services sont la dernière étape pour déterminer où une requête est finalement traitée. Le service api@internal est un service intégré qui se trouve derrière l'API qu'on exposez. Tout comme les routeurs et les middlewares, les services peuvent être configurés dans ce fichier.

# Harbor

Télécharger la version souhaitée de harbor ici : https://github.com/goharbor/harbor/releases

Dans notre cas nous avons pris la version 2.2.0 disponible içi : https://github.com/goharbor/harbor/releases/tag/v2.2.0

Télécharger ensuite harbor-online-installer-v2.2.0.tgz.asc et harbor-online-installer-v2.2.0.tgz

On vérifie ensuite l’authenticité du fichier

``gpg --keyserver hkps://keyserver.ubuntu.com --receive-keys 644FF454C0B4115C``

Le retour console devrait afficher

``public key "Harbor-sign (The key for signing Harbor build) <jiangd@vmware.com>" imported``

``gpg -v –keyserver hkps://keyserver.ubuntu.com –verify harbor-online-installer- v2.2.0.tgz.asc``

Le retour console devrait afficher

``gpg: armor header: Version: GnuPG v1
gpg: assuming signed data in 'harbor-online-installer-v2.0.2.tgz'
gpg: Signature made Tue Jul 28 09:49:20 2020 UTC
gpg:                using RSA key 644FF454C0B4115C
gpg: using pgp trust model
gpg: Good signature from "Harbor-sign (The key for signing Harbor build) <jiangd@vmware.com>" [unknown]``

On vient ensuite extraire le fichier compressé

``tar xvf harbor-online-installer-v2.2.0.tgz``

Accéder ensuite au dossier généré

``cd harbor/``

Copier le fichier harbor.yml.tmpl vers un fichier harbor.yml

``cp harbor.yml.tmpl harbor.yml``

Editer ensuite le fichier créé

``vim harbor.yml``

Il va ensuite falloir commenter la partie sur le https et modifier le port d’écoute http ainsi que le hostname pour qu’il corresponde à votre domaine, dans notre cas nous avons le domaine ynov-lab.tk donc notre harbor aura comme hostname harbor. ynov-lab.tk.

![Harbor1](img/harbor1.PNG)

Une fois le fichier sauvegarder, éxécuter le script d’installation

``sudo ./install.sh``

Une fois le script terminé un docker-file va être généré dans le répertoire.

Editer ce ficher pour qu’il soit pris en  compte par Traefik. 

Rajouter une liste de labels pour renseigné les informations relatives à traefik sur le service proxy.

![Harbor2](img/harbor2.PNG)

Il va aussi falloir supprimer le network créé par défaut pour renseigner celui de traefik.

Puis changer le network de chaque service par celui de traefik.

![Harbor3](img/harbor3.PNG)

Notre harbor est désormais derrière traefik.

# Sonarqube

SonarQube est un outil de révision automatique du code permettant de détecter les bogues, les vulnérabilités et les odeurs de code dans votre code. Il peut s'intégrer à votre flux de travail existant pour permettre une inspection continue du code dans les branches de votre projet et les demandes de retrait.

## Vue d'ensemble

![Sonarqube](img/sonar-overview.png)

Dans un processus de développement typique :

1. Les développeurs développent et fusionnent le code dans un IDE (en utilisant de préférence SonarLint pour recevoir un retour immédiat dans l'éditeur) et enregistrent leur code dans leur ALM.
2. L'outil d'intégration continue (IC) d'une organisation vérifie, construit et exécute les tests unitaires, et un analyseur SonarQube intégré analyse les résultats.
3. L'analyseur envoie les résultats au serveur SonarQube qui fournit un retour d'information aux développeurs par le biais de l'interface SonarQube, d'un courrier électronique, de notifications in-IDE (via SonarLint) et d'une décoration sur les demandes de pull ou de fusion (à partir de l'édition Developer).

## Configuration de SonarQube

```yaml
version: "3"

services:

  db:
    image: postgres:12.1
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=mypass
      - POSTGRES_DB=sonarqube
    volumes:
      - sonarqube_db:/var/lib/postgresql/data
    networks:
      - traefik-proxy
    

  sonarqube:
    image: sonarqube:7.7-community
    environment:
      - sonar.jdbc.username=sonar
      - sonar.jdbc.password=mypass
      - sonar.jdbc.url=jdbc:postgresql://db:5432/sonarqube
    volumes:
      - sonarqube_conf:/opt/sonarqube/conf
      - sonarqube_extensions:/opt/sonarqube/extensions
      - sonarqube_logs:/opt/sonarqube/logs
      - sonarqube_data:/opt/sonarqube/data
    networks:
      - traefik-proxy
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-proxy"
      - "traefik.http.routers.sonarqube.rule=Host(`sonar.ynov-lab.tk`)"
      - "traefik.http.routers.sonarqube.entrypoints=websecure"
      - "traefik.http.routers.sonarqube.tls.certresolver=lets-encrypt"

volumes:
  sonarqube_conf:
  sonarqube_extensions:
  sonarqube_logs:
  sonarqube_data:
  sonarqube_db:

networks:
  traefik-proxy:
    external: true
```

## Configuration de l'analyse

L'analyse de votre code commence par l'installation et la configuration d'un analyseur SonarQube. L'analyseur peut être exécuté lors de votre construction ou dans le cadre de votre pipeline d'intégration continue (CI), et effectuer une analyse chaque fois que votre processus de construction est déclenché.

## Configuration de sonar-scanner dans une CI/CD

```yaml
sonarqube-check:
  image:
    name: sonarsource/sonar-scanner-cli:latest
  variables:
    SONAR_TOKEN: "${SONAR_TOKEN}"
    SONAR_HOST_URL: "https://sonar.ynov-lab.tk"
    SONAR_USER_HOME: "vote-app"
    GIT_DEPTH: 0 # Demande à git de récupérer toutes les branches du projet, requises par la tâche d'analyse.
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  stage: test
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.projectKey=Vote_APP
  allow_failure: true
  only:
    - master
```

# CI/CD avec GitLab

Commencer par créer un runner dédié

Dans un porjet Gitlab aller dans Settings -> CI/CD puis cliquer sur Expand sur la partie Runners

Cliquer ensuite sur "Show Runner installation instructions"

Une fois le runner créer il devrait s'afficher dans la partie Specific runners. Penser aussi à désactiver les Shared runners

![cicd1](img/cicd1.PNG)

Il faut desormais autoriser le user du runner à lancer les commandes docker

``sudo usermod -aG docker gitlab-runner``

Pour tester l'accès

``sudo -u gitlab-runner -H docker info``

La commande docker info doit retourner quelque chose

Maintenant que l'accès est bien configuré il va falloir créer un ficher .gitlab-ci.yml dans notre projet GitLab

![cicd1](img/cicd2.PNG)

Dans la première partie on vient créer des stages, ce sont les différentes étapes de notre pipeline.

Ensuite nous avons créer trois jobs qui viennent build une image docker puis viennent le push sur notre registry harbor.

Le dernier job lui viens lancer l'application à l'aide d'un docker-compose.

On utilise $CI_COMMIT_SHORT_SHA pour versionner nos images. Cette variable retourne le SHA du commit.

# Monitoring

![monitoring](img/monitoring.png)

## cAdvisor 

cAdvisor (Container Advisor) permet aux utilisateurs de conteneurs de comprendre l'utilisation des ressources et les caractéristiques de performance de leurs conteneurs en cours d'exécution. Il s'agit d'un démon en cours d'exécution qui collecte, agrège, traite et exporte des informations sur les conteneurs en cours d'exécution. Plus précisément, pour chaque conteneur, il conserve les paramètres d'isolation des ressources, l'utilisation historique des ressources, les histogrammes de l'utilisation historique complète des ressources et les statistiques du réseau. Ces données sont exportées par conteneur et à l'échelle de la machine.

cAdvisor supporte nativement les conteneurs Docker et devrait supporter tout autre type de conteneur. L'abstraction des conteneurs de cAdvisor est basée sur celle de lmctfy, les conteneurs sont donc intrinsèquement imbriqués hiérarchiquement.

### Configuration cAdvisor

```yaml
  cadvisor:
    image: 'google/cadvisor:latest'
    restart: always
    container_name: cadvisor
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:ro
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev/disk:/dev/disk/:ro
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-proxy"
      - "traefik.http.routers.cadvisor-https.rule=Host(`cadvisor.ynov-lab.tk`)"
      - "traefik.http.routers.cadvisor.entrypoints=websecure"
      - "traefik.http.routers.cadvisor.tls.certresolver=lets-encrypt"
      - "traefik.http.services.cadvisor.loadbalancer.server.port=8080"
    networks:
      - traefik-proxy
```

## Prometheus

Prometheus est un logiciel open source créé par la plateforme musicale SoundCloud, conçu pour monitorer les métriques de fonctionnement des serveurs et créer une gestion d’alertes en fonction de seuils considérés critiques. Cet outil de monitoring enregistre en temps réel ces données à l'aide d'un modèle d'extraction de endpoints HTTP. 

### Configuration prometheus

```yaml
  prometheus:
    image: 'prom/prometheus:latest'
    restart: always
    container_name: prometheus
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-proxy"
      - "traefik.http.routers.prometheus-https.rule=Host(`prometheus.ynov-lab.tk`)"
      - "traefik.http.routers.prometheus.entrypoints=websecure"
      - "traefik.http.routers.prometheus.tls.certresolver=lets-encrypt"
      - "traefik.http.services.prometheus.loadbalancer.server.port=9090"
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
    networks:
      - traefik-proxy
```

## Grafana

Portée par l'américain Grafana Labs, Grafana est une plateforme open source taillée pour la surveillance, l'analyse et la visualisation des métriques IT. Elle est livrée avec un serveur web (écrit en Go) permettant d'y accéder via une API HTTP. Sous licence Apache 2.02, Grafana génère ses graphiques et tableaux de bord à partir de bases de données de séries temporelles (time series database) telles que Graphite, InfluxDB ou OpenTSDB. Cette plateforme est aussi un outil indispensable pour créer des alertes.

Véritable éditeur de dashboards informatiques, Grafana permet également de les partager sous forme de snapshot (ou instantanés) avec d'autres utilisateurs. L'outil intègre par ailleurs un système de gestion des droits d'accès et protège les tableaux de bord des modifications accidentelles. Enfin, il bénéficie d'une application particulière pour Kubernetes. Baptisée Grafana App for Kubernetes, elle suit la performance des architectures applicatives basées sur l'orchestrateur open source via trois dashboards : cluster, node, pod/container et deployment

```yaml
  grafana:
    image: grafana/grafana:7.4.3
    restart: always
    container_name: grafana
    volumes:
      - grafana_data:/var/lib/grafana
      - ./grafana/provisioning:/etc/grafana/provisioning
      - ./config/dashboards:/var/lib/grafana/dashboards
      - ./config/datasources:/etc/grafana/datasources
      - ./config/plugins:/var/lib/grafana/plugins
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-proxy"
      - "traefik.http.routers.grafana-https.rule=Host(`monitoring.ynov-lab.tk`)"
      - "traefik.http.routers.grafana.entrypoints=websecure"
      - "traefik.http.routers.grafana.tls.certresolver=lets-encrypt"
      - "traefik.http.services.grafana.loadbalancer.server.port=3000"
    environment:
      - GF_SECURITY_ADMIN_USER=admin
      - GF_SECURITY_ADMIN_PASSWORD=MyPassword
      - GF_USERS_ALLOW_SIGN_UP=false
    expose:
      - 3000
    networks:
      - traefik-proxy
```

## Exemple de dashboard

![dasbhoard](img/dashboard.png)

# Trivy

Trivy est un scanner de vulnérabilités simple et complet pour les conteneurs et autres artefacts. Il permet de détecter les vulnérabilités des paquets du système d'exploitation (Alpine, RHEL, CentOS, etc.) et des dépendances des applications (Bundler, Composer, npm, yarn, etc.). Avant de pousser vers un registre de conteneurs ou de déployer votre application, vous pouvez scanner votre image de conteneur local et d'autres artefacts facilement vous donnant la confiance que tout va bien avec votre application sans configurations plus stressantes à utiliser comme d'autres scanners.

## Configuration trivy

```yaml
  harbor-scanner-trivy:
    image: "aquasec/harbor-scanner-trivy:0.9.0"
    container_name: trivy-adapter
    ports:
      - "8181:8181"
    environment:
      - "HTTP_PROXY=http://nginx:8080"
      - "HTTPS_PROXY=http://nginx:8080"
      - "NO_PROXY=.local,portal,clair-adapter,chartmuseum,trivy-adapter,core,redis"
      - "SCANNER_LOG_LEVEL=trace"
      - "SCANNER_TRIVY_DEBUG_MODE=true"
      - "TRIVY_NON_SSL=true"
      - "SCANNER_API_SERVER_ADDR=:8184"
      - "SCANNER_STORE_REDIS_URL=redis://redis:6379"
      - "SCANNER_JOB_QUEUE_REDIS_URL=redis://redis:6379"
    network_mode: traefik-proxy
```

## Resultat

![Trivy-CVE](img/cve.png)
